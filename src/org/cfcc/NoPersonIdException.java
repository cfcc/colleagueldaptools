/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.cfcc;

/**
 * Custom exception class thrown by the LDAP service when there is an error
 * retrieving the employeeNumber field.
 */
public class NoPersonIdException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7511808486903114539L;
	/** Holds the UID that the error happend on. */
    private String uid = "";
    /** Additional information about the error. */
    private String msg = "";
    /** The original exception, saved for later traceback. */
    public Throwable orig = null;

    /** Class default constructor */
    public NoPersonIdException(String s) {
        super(s);
    }

    /**
     * Class constructor to set a custom error message and the UID for which
     * the error occured.
     * @param message a custom message describing what happend
     * @param uid the uid that was used in the LDAP search
     */
    public NoPersonIdException(String message, String uid) {
        super(message);
        this.uid = uid;
    }

    /**
     * Class constructor that sets the custom message, uid, and the system
     * error message.
     * @param message a custom message describing the error
     * @param uid the search field that had the error
     * @param system_message the error message returned by the previous
     * exception (specifically by Exception.getMessage())
     */
    public NoPersonIdException(String message,
            String uid,
            String system_message) {
        super(system_message);
        this.uid = uid;
        this.msg = message;
    }

    public NoPersonIdException(String msg, String uid, Throwable orig) {
        super(msg);
        this.uid = uid;
        this.orig = orig;
    }

    /**
     * Returns the detailed error message.
     * @return a string with the error message
     */
    @Override
	public String toString() {
        StringBuffer m = new StringBuffer("Error getting person ID for ");
        String s = super.getMessage();
        m.append(uid);
        if (!msg.equals("")) {
            m.append(", " + msg);
        }
        if (!s.equals("")) {
            m.append(": " + s);
        }
        return m.toString();
    }
}
