/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This is a custom exception class that is thrown when the LDAP directory
 * service will not set the password since it does not fit the required
 * constraints (such as minimum number of characters, including a special
 * character, etc.).
 */
package org.cfcc;

/**
 * Error indicating that the user's account in LDAP has been locked by an
 * administrator.
 * 
 * @author Jakim Friant
 */
public class LockedAccountException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1681700204150321973L;

	@Override
	public String toString() {
        return "LDAP account has been administratively locked!";
    }
}
