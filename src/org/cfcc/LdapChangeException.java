/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.cfcc;

/**
 * Custom exception class thrown when the password cannot be saved to LDAP.
 * This can happen when either the server has crashed or if it is in read-only
 * mode.
 * <p>
 * When this exception is caught, the user should be notified that the service
 * is unavaiable and to try again later.
 */
public class LdapChangeException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6710621674729483062L;
	/** holds the DN for which the error occured */
    public String dn = "";
    /** holds the reason for the error (in addition to the LDAP message) */
    public String msg = "";
    
    /**
     * Class default constructor
     *
     * @param s  the error message
     */
    public LdapChangeException(String s) {
        super(s);
    }
    
    /**
     * Class constructor to store the last DN that had the error.
     *
     * @param error_dn  the dn that had the error
     * @param error_msg any additional error messages
     */
    public LdapChangeException(String error_dn, String error_msg) {
        msg = error_msg;
        dn = error_dn;
    }
    
    public LdapChangeException(String msg, Throwable ex) {
        super(msg, ex);
    }
    
    /**
     * Returns a custom error message with the DN, reason, and LDAP error
     * message.
     *
     * @return the string with the error message
     */
    @Override
	public String toString() {
        return "Error updating LDAP for " + dn + ", " + msg + ": " + super.getMessage();
    }
}
