/**
 * 
 */
package org.cfcc.provider;

import asjava.uniobjects.UniFile;
import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSession;

/**
 * @author jfriant
 *
 */
public class UnidataField implements FieldData {
	
	private String description = null;
	private String help = null;
	private int length = 0;
	private String filename = null;
	private int location = 0;
	private UniSession current_session = null;
	private String record_id = null;
	
	public UnidataField() {	}
	

	public String getRecordId() {
		return record_id;
	}

	public void setRecordId(String record_id) {
		this.record_id = record_id;
	}

	public UniSession getCurrentSession() {
		return current_session;
	}

	public void setCurrentSession(UniSession current_session) {
		this.current_session = current_session;
	}

	/* (non-Javadoc)
	 * @see org.cfcc.provider.FieldData#getDescription()
	 */
	@Override
	public String getDescription() {
		return this.description;
	}

	/* (non-Javadoc)
	 * @see org.cfcc.provider.FieldData#getLength()
	 */
	@Override
	public int getLength() {
		return this.length;
	}

	/* (non-Javadoc)
	 * @see org.cfcc.provider.FieldData#getAnswer()
	 */
	@Override
	public String getAnswer() {
		String result = "";
		UniFile fd = null;
		try {
			fd = new UniFile(current_session, filename, 0);
			try {
				result = fd.readField(record_id, location).toString();
			} catch (UniFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (UniFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.cfcc.provider.FieldData#getHelp()
	 */
	@Override
	public String getHelp() {
		return this.help;
	}

	@Override
	public void setDescription(String value) {
		description = value;
		
	}

	@Override
	public void setLength(int value) {
		length = value;
	}

	@Override
	public void setHelp(String value) {
		help = value;
	}

}
