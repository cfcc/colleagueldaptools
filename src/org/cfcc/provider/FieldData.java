/**
 * 
 */
package org.cfcc.provider;

/**
 * @author jfriant
 *
 */
public interface FieldData {
	
	void setDescription(String value);

	String getDescription();
	
	void setLength(int value);
	
	int getLength();
	
	String getAnswer();
	
	void setHelp(String value);
	
	String getHelp();
}
