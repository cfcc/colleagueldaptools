/*
 * Copyright 2007-2011 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.cfcc.colleague;

import asjava.uniclientlibs.*;
import asjava.uniobjects.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.cfcc.CryptoException;
import org.cfcc.utils.Crypto;

/**
 * Base class to encapsulate a connection to the Colleague server.
 *
 * You should subclass this to provide specific methods for data retrieval.
 *
 * @author Jakim Friant
 * @see Registry
 * @see Remote
 */
public class Account {

    /** The host name or IP address of the Colleague server */
    String server_address = "";
    /** A user account with UniData colon prompt access on the server */
    String username = "";
    /** The password for the UniData user account */
    String passwd = "";
    /** The session object used to open the connection, open files, etc. */
    protected UniSession uSession = null;
    /** UniData record lock code used when writing */
    public static final int EXCLUSIVE_UPDATE = 1;
    /** The number of attempts to make when trying to write to a locked
     * record */
    public static final int MAX_ATTEMPTS = 3;
    /** The number of seconds before the UniRPC connection times out. */
    public static final int RPC_TIMEOUT = 180;
    /** The number of milliseconds to wait before checking a locked record
     * again */
    public static final int RECORD_LOCK_WAIT = 3000;

    /**
     * Constructor that sets the initial username and password for connecting to
     * the given server.
     *
     * @param server The DNS name or IP address of the server with the
     * UniData database
     * @param uname  the username to use when connecting, which needs only
     * UniData colon prompt access.
     * @param pswd   the password that matches the username.
     */
    public Account(String server, String uname, String pswd) {
        server_address = server;
        username = uname;
        passwd = pswd;
    }

    /**
     * Set up the initial connection parameters and establish the connection.
     * @param account_path The Unix path to the UniData account (ex:
     * /datatel/work/coltest).
     * @exception UniSessionException If connecting to the server fails.
     */
    public void connect(String account_path) throws UniException {
        uSession = new UniSession();

        // check for an encrypted password
        if (passwd.startsWith(Crypto.prefix())) {
            Crypto c = new Crypto();
            try {
                passwd = c.decrypt(passwd);
            } catch (CryptoException ex) {
                throw new UniException("Unable to decrypt password", UniException.UNISESSION_EXCEPTION);
            }
        }

        // set up the connection information
        uSession.setHostName(server_address);
        uSession.setUserName(username);
        uSession.setPassword(passwd);
        uSession.setAccountPath(account_path);

        // set up encryption if requested
        

        // This seems to only set the timeout on commands sent after the 
        // connection has been established.
        uSession.setTimeout(RPC_TIMEOUT);

        // This appears to be working to limit the amount of time waiting
        // for a connection.
        uSession.connection.setTimeoutSeconds(RPC_TIMEOUT);

        // connect to the server
        uSession.connect();

    /*********************************
     * DISABLED for R18 since now the ENVINIT/SQL.ENVINIT subroutines require a
     * DMI connection to be established -- and we don't want to go with a single
     * user used to connect since that causes issues with identifying record locks.
     * 
    try {
    // initialize the Envision environment
    UniCommand uCommand = uSession.command("ENVINIT");
    uCommand.exec();
    UniCommand uCommand1 = uSession.command("ST.JUMPSTART");
    uCommand.exec();
    }
    catch (UniCommandException e) {
    System.err.println("ResetPassword: Unable to initialize Envision: " + e);
    }
     **********************************/
    }

    /**
     * Convenience function to open a UniFile and return the file handle object.
     * @param filename a filename to open
     * @return a UniFile object
     * @throws UniSessionException If there is an error opening the file
     */
    public UniFile openUniFile(String filename) throws UniSessionException {
        return uSession.open(filename);
    }

    /**
     * Convenience function to create a command handle.
     * @return a UniCommand object
     * @param cmd A UniVerse command string or stored procedure name
     * @throws asjava.uniobjects.UniSessionException If there is an error establishing the command object
     */
    public UniCommand newCommand(String cmd) throws UniSessionException {
        return uSession.command(cmd);
    }

    /**
     * Convenience function to create a new select list.
     * @param which List number to use
     * @throws asjava.uniobjects.UniSelectListException is thrown if there is an error creating the list
     * @return a select list object
     */
    public UniSelectList newSelectList(int which) throws UniSelectListException {
        return new UniSelectList(uSession, which);
    }

    public UniString convertDate(UniString date_in) {
        UniString date_out = null;
        try {
            date_out = uSession.oconv(date_in, "D2/");
        } catch (UniStringException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, "Unable to convert date", ex);
        }
        return date_out;
    }

    /**
     * Loads and returns an existing saved list in the account.
     *
     * @param sl_name name of an existing list in SAVEDLISTS
     * @return A select list object with the records for the given savedlist
     */
    public UniSelectList getList(String sl_name) {
        UniSelectList sl = null;
        try {
            sl = new UniSelectList(uSession, 1);
            sl.getList(sl_name);
        } catch (UniSelectListException e) {
            System.err.println("Error trying to get select list " + sl_name + ": " + e);
        }
        return sl;
    }

    /**
     * Closes the UniData session
     */
    public void disconnect() {
        try {
            uSession.disconnect();
        } catch (UniSessionException e) {
            System.err.println("ResetPassword: Error closing session: " + e);
        }
    }
}