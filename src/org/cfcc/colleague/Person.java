/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.colleague;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.ListColumn;
import org.cfcc.colfile.StringColumn;
import org.cfcc.colfile.Table;

/**
 * Database file wrapper for the PERSON file in Colleague/UniData
 * @author jfriant
 */
public class Person extends Table {

    StringColumn pref_addr;
    StringColumn ssn;
    StringColumn birth_date;
    ListColumn where_used;
    StringColumn last_name;
    StringColumn first_name;

    /**
     * Class constructor to initialze the fields and save the database connection.
     * @param s a current UniObjects session object
     */
    public Person(UniSession s) {
        super(s, "PERSON");
        pref_addr = new StringColumn(this, 6);
        ssn = new StringColumn(this, 8);
        birth_date = new StringColumn(this, 14, "D4/");
        where_used = new ListColumn(this, 31);
        last_name = new StringColumn(this, 1);
        first_name = new StringColumn(this, 3);
    }
}
