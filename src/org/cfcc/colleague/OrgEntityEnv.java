/*
 * OrgEntityEnv.java
 *
 * Created on May 16, 2007, 8:51 AM
 *
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.colleague;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.*;

/**
 * Holds a record from the ORG.ENTITY.ENV file.  It has been expanded to include
 * data from the ORG.ENTITY file, such as last and first names.
 * @author jfriant
 */
public class OrgEntityEnv extends Table {

    public StringColumn username;
    public StringColumn oee_id;
    public TransColumn last;
    public TransColumn first;
    public StringColumn person_id;
    public StringColumn password;
    public StringColumn password_chg_date;
    public StringColumn password_chg_time;
    public StringColumn password_expire_date;
    public StringColumn no_invalid_logins;
    public StringColumn invalid_login_date;
    public StringColumn invalid_login_time;
    public final static int OEE_ID = 0;
    public final static int OEE_NO_INVALID_LOGINS = 25;
    public final static int OEE_OPERS = 30;
    public final static int OEE_PASSWORD = 18;
    public final static int OEE_PASSWORD_CHG_DATE = 31;
    public final static int OEE_PASSWORD_CHG_TIME = 32;
    public final static int OEE_PASSWORD_EXPIRE_DATE = 21;
    public final static int OEE_PASWORD_HINT = 19;
    public final static int OEE_PRIOR_PSWD1 = 27;
    public final static int OEE_PRIOR_PSWD2 = 28;
    public final static int OEE_PRIOR_PSWD3 = 29;
    public final static int OEE_RESOURCE = 15;
    public final static int OEE_USERNAME = 17;
    public final static int OEE_INVALID_LOGIN_DATE = 35;
    public final static int OEE_INVALID_LOGIN_TIME = 36;

    /** Creates a new instance of OrgEntityEnv */
    public OrgEntityEnv(UniSession uSession) {
        super(uSession, "ORG.ENTITY.ENV");
        username = new StringColumn(this, OEE_USERNAME);
        oee_id = new StringColumn(this, OEE_ID);
        person_id = new StringColumn(this, OEE_RESOURCE);
        password = new StringColumn(this, OEE_PASSWORD);
        password_chg_date = new StringColumn(this, OEE_PASSWORD_CHG_DATE);
        password_chg_time = new StringColumn(this, OEE_PASSWORD_CHG_TIME);
        password_expire_date = new StringColumn(this, OEE_PASSWORD_EXPIRE_DATE);
        no_invalid_logins = new StringColumn(this, OEE_NO_INVALID_LOGINS);
        invalid_login_date = new StringColumn(this, OEE_INVALID_LOGIN_DATE);
        invalid_login_time = new StringColumn(this, OEE_INVALID_LOGIN_TIME);
        first = new TransColumn(this, OEE_RESOURCE, "PERSON", 3);
        last = new TransColumn(this, OEE_RESOURCE, "PERSON", 1);
    }
}
