/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.cfcc.colleague;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.StringColumn;
import org.cfcc.colfile.Table;

/**
 *
 * @author jfriant
 */
public class Address extends Table {

    /** Length of the zip code without the extended portion. */
    @SuppressWarnings("unused")
	private static final int ZIP_LEN = 5;
    StringColumn zip;

    public Address(UniSession s) {
        super(s, "ADDRESS");
        zip = new StringColumn(this, 1);
    }
}
