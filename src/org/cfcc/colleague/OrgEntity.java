/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.cfcc.colleague;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.*;

/**
 * Datbase file wrapper for the ORG.ENTITY Colleague file.
 * @author jfriant
 */
public class OrgEntity extends Table {

    /** Field location of the last name  */
    public static final int OE_LAST_NAME = 3;
    /** Field location of the first name  */
    public static final int OE_FIRST_NAME = 4;
    /** Field location of the ORG.ENTITY.ENV foreign key */
    public static final int OE_ORG_ENTITY_ENV = 14;

    /** Database field wrapper object */
    public StringColumn first_name;
    /** Database field wrapper object */
    public StringColumn last_name;
    /** Database field wrapper object */
    public StringColumn org_entity_env;

    /**
     * Class constructor to bind to an established connection and initialize the fields.
     * @param s a current UniObjects session object
     */
    public OrgEntity(UniSession s) {
        super(s, "ORG.ENTITY");
        first_name = new StringColumn(this, OE_FIRST_NAME);
        last_name = new StringColumn(this, OE_LAST_NAME);
        org_entity_env = new StringColumn(this, OE_ORG_ENTITY_ENV);
    }
}
