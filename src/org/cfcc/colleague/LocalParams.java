/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.colleague;

import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSession;
import asjava.uniobjects.UniSessionException;
import org.cfcc.colfile.StringColumn;
import org.cfcc.colfile.Table;

/**
 *
 * @author jfriant
 */
public class LocalParams extends Table {

    /** Field location of the USER4 field in the PERSON file, stores the answer */
    public static final int LOC_USER4 = 98;
    /** Field locatoin of the USER5 field in the PERSON file, stores the question */
    public static final int LOC_USER5 = 101;
    /** Field location of the saved question in the local params file */
    public static final int LOC_X810_P_QUESTION = 1;
    /** Field location of the answer in local params file */
    public static final int LOC_X810_P_ANSWER = 2;
    /** Field location of the address (NOT USED) */
    public static final int LOC_X810_P_ADDRESS = 3;
    /** Field location of the user name (NOT USED) */
    public static final int LOC_X810_P_UNAME = 4;
    /** Field location of the password */
    public static final int LOC_X810_P_PASSWD = 5;
    /** member used to access the X810.P.QUESTION field in UniData */
    public StringColumn question;
    /** member that access the encrypted answer field in UniData */
    public StringColumn answer;
    /** Private member used to store the password history in user records */
    protected StringColumn password;

    /**
     * Constructor that sets up the Local Parameter file to use PERSON or a
     * PERSON co-file.  If the person file is used, the password field is
     * disabled by setting it to null.
     * @param s the UniSession object with connection information
     * @param filename the UniData name of the parameter file
     */
    public LocalParams(UniSession s, String filename) {
        super(s, filename);
        if (filename.equals("PERSON")) {
            question = new StringColumn(this, LOC_USER4);
            answer = new StringColumn(this, LOC_USER5);
            password = null;
        } else {
            question = new StringColumn(this, LOC_X810_P_QUESTION);
            answer = new StringColumn(this, LOC_X810_P_ANSWER);
            password = new StringColumn(this, LOC_X810_P_PASSWD);
        }
    }

    /**
     * Accessor for the password field which may be null, returns an empty string if that is the case.
     * @return the value of the password field or an empty string
     * @throws UniFileException if there is a UniData error acessing the field
     */
    public String getPassword() throws UniFileException, UniSessionException {
        String result = "";
        if (password != null) {
            result = password.get().toString();
        }
        return result;
    }

    /**
     * Sets the password field to a new value, does nothing if the field is not being used.
     * @param passwd_in the new password to save
     * @throws UniFileException if there is a UniData error acessing the field
     */
    public void setPassword(String passwd_in) throws UniFileException, UniSessionException {
        if (password != null) {
            password.set(passwd_in);
        }
    }
}
