/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.colleague;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.ListColumn;
import org.cfcc.colfile.Table;

/**
 * Database file wrapper for the STAFF table.
 * @author jfriant
 */
public class Staff extends Table {

    /** Multi-valued field with the office codes that are set on the SVM form */
    ListColumn office_code;

    /**
     * Constructor to bind to an open session and initialze the field variables.
     * @param session a current UniObjects session
     */
    public Staff(UniSession session) {
        super(session, "STAFF");
        office_code = new ListColumn(this, 6);
    }
}
