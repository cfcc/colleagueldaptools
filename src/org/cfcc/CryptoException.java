/*
 * CryptoException.java
 *
 * Created on December 27, 2007, 9:30 AM
 *
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.cfcc;

/**
 * This custom exception is thrown when there is an error encrypting or
 * decrypting in the custom Crypto package.
 * @author jfriant
 */
public class CryptoException extends Exception {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 6814943762249501293L;
	/** Creates a new instance of CryptoException */
    public CryptoException() {
        super();
    }
    
    public CryptoException(String s) {
        super(s);
    }
    
    public CryptoException(String msg, Throwable ex) {
        super(msg, ex);
    }
    
    @Override
	public String toString() {
        String result = "CryptoException: " + super.getMessage() +
                " due to " + super.getCause().getMessage();
        return result;
    }
    
}