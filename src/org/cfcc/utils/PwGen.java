/**
 * A simple random password generator, user can specify the length.
 * Default length is 8, the common unix system effective password length.
 *
 * Downloaded from:
 * <http://bobcat.webappcabaret.net/javachina/jc/share/PwGen.htm>
 *
 * last updated: 01-04-2003
 *
 * Copyright © 1999 - 2003 Roseanne Zhang, All Rights Reserved
 *
 * @author Roseanne Zhang
 */
package org.cfcc.utils;
 
public class PwGen {
    /**
     * Returns a random generated password with numbers and upper and lower
     * case letters.
     *
     * @param n length of password this function should return
     * @return pw the generated password string
     */
    public static String getPassword(int n) {
	char[] pw = new char[n];
	int c  = 'A';
	int  r1 = 0;
	for (int i=0; i < n; i++)
	{
	    r1 = (int)(Math.random() * 3);
	    switch(r1) {
		case 0: c = '0' +  (int)(Math.random() * 10); break;
		case 1: c = 'a' +  (int)(Math.random() * 26); break;
		case 2: c = 'A' +  (int)(Math.random() * 26); break;
	    }
	    pw[i] = (char)c;
	}
	return new String(pw);
    }
    
    /**
     * Prints a random password.
     *
     * @param args the first optional argument is the password length
     */
    public static void main(String[] args) {
	int len = 8; // default 8 on unix, more is useless
	
	if (args.length >= 1)
	{
	    try
	    {
		len = Integer.parseInt(args[0]);
	    }
	    catch (NumberFormatException nfe){
	    }
	}
	System.out.println(getPassword(len));
    }
}