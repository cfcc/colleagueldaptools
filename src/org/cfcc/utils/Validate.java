/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.utils;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * A class with functions to clean up and sanitize input strings.  Can be
 * substantiated as an object for validating passwords.
 * 
 * @author Jakim Friant
 */
public class Validate {

    public final static int NO_MAX = -1;
    private final static int DEFAULT_MIN_LEN = 6;
    private final static int DEFAULT_MAX_LEN = 20;
    private final static int DEFAULT_MIN_STR = 2;
    private final static boolean NO_SPECIAL_CHARS = false;
    private int min_length;
    private int max_length;
    private int min_strength;

    /**
     * Initialize the class with default settings only.
     */
    public Validate() {
        min_length = DEFAULT_MIN_LEN;
        max_length = DEFAULT_MAX_LEN;
        min_strength = DEFAULT_MIN_STR;
    }

    /**
     * Set up the object to validate passwords.  The limits can be changed by
     * passing a properties object with the following keys:
     * 
     * <ul>
     *  <li>servlet.checkPassword.min_length</li>
     *  <li>servlet.checkPassword.max_length</li>
     *  <li>servlet.checkPassword.min_strength</li>
     * </ul>
     * 
     * @param props a Properties object with keys defining checkPassword limits.
     */
    public Validate(Properties props) {
        try {
            min_length = Integer.parseInt(props.getProperty("servlet.password.min_length", String.valueOf(DEFAULT_MIN_LEN)));
        } catch (NumberFormatException e) {
            min_length = DEFAULT_MIN_LEN;
        }
        try {
            max_length = Integer.parseInt(props.getProperty("servlet.password.max_length", String.valueOf(DEFAULT_MAX_LEN)), DEFAULT_MAX_LEN);
        } catch (NumberFormatException e) {
            max_length = DEFAULT_MAX_LEN;
        }
        try {
            min_strength = Integer.parseInt(props.getProperty("servlet.password.min_strength", String.valueOf(DEFAULT_MIN_STR)));
        } catch (NumberFormatException e) {
            min_strength = DEFAULT_MIN_STR;
        }
    }

    public void setMinLength(int len) {
        min_length = len;
    }

    public int getMinLength() {
        return min_length;
    }

    public void setMaxLength(int len) {
        max_length = len;
    }

    public int getMaxLength() {
        return max_length;
    }

    public void setMinStrength(int strength) {
        min_strength = strength;
    }

    public int getMinStrength() {
        return min_strength;
    }

    public boolean noSpecial() {
        return NO_SPECIAL_CHARS;
    }

    /**
     * Sanitizes the input strings coming from a web page.  Gets rid of
     * everything but letters, numbers, spaces, and dashes.
     *
     * @param str_in the input string to be sanitized
     * @return the sanitized string
     */
    public static String sanitize(String str_in) {
        Character c;
        Character space = new Character(' ');
        Character dash = new Character('-');
        StringBuffer str = new StringBuffer("");
        if (str_in != null) {
            str.append(str_in);
            for (int i = str.length() - 1; i >= 0; i--) {
                c = new Character(str.charAt(i));
                if (!(c.equals(dash) || c.equals(space))) {
                    if (!Character.isLetterOrDigit(c.charValue())) {
                        str.deleteCharAt(i);
                    }
                }
            }
        }
        return str.toString();
    }

    /** 
     * Sanitizes the string but limits the maximum length returned.
     *
     * @param str_in the input string to sanitize
     * @param max_len the maximum number of characters to return
     * @return a string with only alphanumeric characters
     */
    public static String sanitize(String str_in, int max_len) {
        String str = sanitize(str_in);
        if (str.length() > max_len) {
            try {
                str = str.substring(0, max_len);
            } catch (StringIndexOutOfBoundsException e) {
                System.err.println("ResetPassword: Validate error: " + e);
            }
        }
        return str;
    }
    
    /**
     * Return true if a parameter string is present.
     * @param params_in Enumeration with each parameter name
     * @return a Boolean object with true or false
     */
    public static Boolean checkForParam(Enumeration<?> params_in, String param_to_check_for) {
    	Boolean result = new Boolean(false);
    	while (params_in.hasMoreElements()) {
    		String param = (String) params_in.nextElement();
        	if (param.equals(param_to_check_for)) {
        		result = true;
        	}
    	}
    	return result;
    }

    /**
     * Returns only the numbers from the input string.
     *
     * FIXME: the only way I know right now to give default values is by
     * creating two functions with differing arguments that call a third
     * function that actually does the work.
     *
     * @param in the input string
     * @return an output string with only digits
     */
    public static String numbersOnly(String in) {
        return getNumbers(in, NO_MAX);
    }

    /**
     * Returns only digits from the input string up to a maximum length.
     *
     * @param in the input string
     * @param max_len the maximum number of character to return, if it is -1
     * then the whole string is returned
     * @return an output string with upto the maximum number of digits
     */
    public static String numbersOnly(String in, int max_len) {
        return getNumbers(in, max_len);
    }

    /**
     * Only returns the numbers found in the input string.
     *
     * @param in the input string
     * @param max_len if it is -1 then the whole string is returned, otherwise
     * just a sub string up to max_length in length
     * @return the output string with the digits, or an empty string
     * @see #numbersOnly(String)
     * @see #numbersOnly(String,int)
     */
    private static String getNumbers(String in, int max_len) {
        StringBuffer out = new StringBuffer("");
        if (in != null) {
            for (int i = 0; i < in.length(); i++) {
                if (Character.isDigit(in.charAt(i))) {
                    out.append(in.charAt(i));
                }
            }
            if (max_len > NO_MAX) {
                try {
                    out = new StringBuffer(out.substring(0, max_len));
                } catch (Exception e) {
                }
            }
        }
        return out.toString();
    }

    /**
     * Returns an array of failed validation checks for the checkPassword or an empty
     * array if it passed.  The tunable properties that can be set are as
     * follows:
     *
     * <ul>
     *  <li>servlet.checkPassword.min_length</li>
     *  <li>servlet.checkPassword.max_length</li>
     *  <li>servlet.checkPassword.min_strength</li>
     * </ul>
     *
     * The strength setting is based on some arbitrary values to determine what
     * combination of characters is required:
     *
     * 1 = lowercase letters required
     * 2 = as above plus at least one number
     * 3 = as above plus at least one uppercase letter
     * 4 = as above plus at least one special character
     *
     * @param passwd - the checkPassword string
     * @param passwd_check - the second checkPassword string that is required to see if they match
     * @param props - a Properties object with the checkPassword requirements
     * @return
     */
    public ArrayList<String> checkPassword(String passwd, String passwd_check) {
        ArrayList<String> errors = new ArrayList<String>();

        Pattern re_lowercase = Pattern.compile("[a-z]");
        Pattern re_uppercase = Pattern.compile("[A-Z]");
        Pattern re_numbers = Pattern.compile("[0-9]");
        Pattern re_special = Pattern.compile("\\W");

        if (!passwd.equals(passwd_check)) {
            errors.add("Passwords do not match");
        } else {
            if (passwd.length() < min_length) {
                errors.add("Password is too short, must be at least " + String.valueOf(min_length) + " characters long");
            }
            if (passwd.length() > max_length) {
                errors.add("Password is too long, cannot be more than " + String.valueOf(max_length) + " characters long");
            }
            if (re_lowercase.matcher(passwd).find() == false && min_strength >= 1) {
                errors.add("You must use at least one lower case letter");
            }
            if (re_numbers.matcher(passwd).find() == false && min_strength >= 2) {
                errors.add("You must use at least one number");
            }
            if (re_uppercase.matcher(passwd).find() == false && min_strength >= 3) {
                errors.add("You must use at least one upper case letter");
            }
            if (re_special.matcher(passwd).find() == false && min_strength >= 4) {
                errors.add("You must use at least one special character");
            }
        }
        return errors;
    }
    
	/**
	 * Reformats the SSN by stripping out all but the numbers and adding the
	 * dashes.
	 * 
	 * @param ssn_in
	 *            a string with the ssn in any format (as long as it has all 9
	 *            numbers in the correct sequence)
	 * @return a string with the ssn in the format NNN-NN-NNNN
	 */
	public static String formatSsn(String ssn_in) {
		String ssn_out = "";
		ssn_in = numbersOnly(ssn_in, 9);
		if (ssn_in.length() == 9) {
			ssn_out = ssn_in.substring(0, 3) + "-" + ssn_in.substring(3, 5)
					+ "-" + ssn_in.substring(5);
		}
		return ssn_out;
	}
}
