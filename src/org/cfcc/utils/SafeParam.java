/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.utils;

/**
 * Converts incoming parameter strings to a safe equivalent.  Adds functionality
 * that the Validate class does not have by being able to compare the sanitized
 * string with the original and warn the user of any differences.
 */
public class SafeParam {

    String original = null;
    String safe_param = null;
    int max_length = Validate.NO_MAX;

    /**
     * Default constructor that creates a blank class.
     */
    public SafeParam() {
        original = "";
    }

    /**
     * Constructor that takes a basic string parameter.
     * @param param The incoming CGI/web parameter that will need to be saniitzed.
     */
    public SafeParam(String param) {
        original = param;
    }

    /**
     * Constructor that takes a parameter string that is limited to a maximum length.
     * @param param The incoming parameter string that must be sanitized.
     * @param max_len The maximum length that the sanitized string should have.
     */
    public SafeParam(String param, int max_len) {
        original = param;
        max_length = max_len;
    }

    /**
     * Returns the sanitized parameter as a alphanumeric string.
     * @return The alphanumeric parameter after being sanitized.
     */
    @Override
	public String toString() {
        if (safe_param == null) {
            if (max_length == Validate.NO_MAX) {
                safe_param = Validate.sanitize(original);
            } else {
                safe_param = Validate.sanitize(original, max_length);
            }
        }
        return safe_param;
    }

    /**
     * Returns only the numbers found in the original parameter string.
     * @return The sanitized parameter string with numbers only.
     */
    public String toNumbers() {
        if (safe_param == null) {
            if (max_length == Validate.NO_MAX) {
                safe_param = Validate.numbersOnly(original);
            } else {
                safe_param = Validate.numbersOnly(original, max_length);
            }
        }
        return safe_param;
    }

    /**
     * Compares the original and sanitized parameters to see if anything changed.
     * @return The difference in length between the original and sanitized strings, or -1 if the parameter has not been sanitized yet.
     */
    public int compareLen() {
        int len_out = -1;
        if (safe_param != null) {
            len_out = original.length() - safe_param.length();
        }
        return len_out;
    }
}
