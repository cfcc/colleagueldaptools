/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.utils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Starts a SSL connection without verifying the server's certificate.  This
 * should only be used when connecting to a known server (Active Directory
 * (LDAP) or HTTPS) and is intended to be used with local self-signed
 * certificates, especially when there is trouble loading the
 * Certificate Authority (CA) files so validation could be completed through
 * the normal process.  This is based on sample code downloaded from
 * <a href="http://blog.platinumsolutions.com/node/79">Platinum Solutions'</a>
 * web site.
 *
 * @author Mike McKinney, Platinum Solutions, Inc.
 */
public class BlindSSLSocketFactory extends SocketFactory {

    private static SocketFactory blindFactory = null;

    /**
     * Builds an all trusting "blind" ssl socket factory.
     */
    static {
        // create a trust manager that will purposefully fall down on the
        // job
        TrustManager[] blindTrustMan = new TrustManager[]{new X509TrustManager() {

        @Override
		public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        @Override
		public void checkClientTrusted(X509Certificate[] c, String a) {
        }

        @Override
		public void checkServerTrusted(X509Certificate[] c, String a) {
        }
    }};

        // create our "blind" ssl socket factory with our lazy trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, blindTrustMan, new java.security.SecureRandom());
            blindFactory = sc.getSocketFactory();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see javax.net.SocketFactory#getDefault()
     */
    public static SocketFactory getDefault() {
        return new BlindSSLSocketFactory();
    }

    /**
     * @see javax.net.SocketFactory#createSocket(java.lang.String, int)
     */
    @Override
	public Socket createSocket(String arg0, int arg1) throws IOException,
            UnknownHostException {
        return blindFactory.createSocket(arg0, arg1);
    }

    /**
     * @see javax.net.SocketFactory#createSocket(java.net.InetAddress, int)
     */
    @Override
	public Socket createSocket(InetAddress arg0, int arg1) throws IOException {
        return blindFactory.createSocket(arg0, arg1);
    }

    /**
     * @see javax.net.SocketFactory#createSocket(java.lang.String, int,
     *      java.net.InetAddress, int)
     */
    @Override
	public Socket createSocket(String arg0, int arg1, InetAddress arg2, int arg3)
            throws IOException, UnknownHostException {
        return blindFactory.createSocket(arg0, arg1, arg2, arg3);
    }

    /**
     * @see javax.net.SocketFactory#createSocket(java.net.InetAddress, int,
     *      java.net.InetAddress, int)
     */
    @Override
	public Socket createSocket(InetAddress arg0, int arg1, InetAddress arg2,
            int arg3) throws IOException {
        return blindFactory.createSocket(arg0, arg1, arg2, arg3);
    }
}
