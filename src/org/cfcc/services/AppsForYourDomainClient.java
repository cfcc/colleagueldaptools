/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cfcc.services;

import com.google.gdata.client.appsforyourdomain.UserService;
import com.google.gdata.data.appsforyourdomain.AppsForYourDomainException;
import com.google.gdata.data.appsforyourdomain.Login;
import com.google.gdata.data.appsforyourdomain.provisioning.UserEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.acegisecurity.providers.encoding.ShaPasswordEncoder;
import org.cfcc.CryptoException;
import org.cfcc.utils.Crypto;

/**
 * This client is based on the sample code from Google for their Apps client.
 * This client is only used to update the password in Gmail when LDAP is
 * updated, and avoid a long delay between the change and the next Directory
 * Sync, which can be up to a week.
 *
 * @author jfriant
 */
public class AppsForYourDomainClient {

    private static final Logger LOGGER = Logger.getLogger(
            AppsForYourDomainClient.class.getName());
    private static final String APPS_FEEDS_URL_BASE =
            "https://apps-apis.google.com/a/feeds/";
    protected static final String SERVICE_VERSION = "2.0";
    protected final String domainUrlBase;
    protected UserService userService;
    protected final String domain;

    protected AppsForYourDomainClient(String domain) {
        this.domain = domain;
        this.domainUrlBase = APPS_FEEDS_URL_BASE + domain + "/";
    }

    /**
     * Constructs an AppsForYourDomainClient for the given domain using the
     * given admin credentials.
     *
     * @param adminEmail An admin user's email address such as admin@domain.com
     * @param adminPassword The admin's password
     * @param domain The domain to administer
     */
    public AppsForYourDomainClient(String adminEmail, String adminPassword,
            String domain) throws CryptoException, AuthenticationException {
        this(domain);

        // check for an encrypted password
        if (adminPassword.startsWith(Crypto.prefix())) {
            Crypto c = new Crypto();
            adminPassword = c.decrypt(adminPassword);
        }

        // Configure all of the different Provisioning services
        userService = new UserService("CFCC-ResetPassword-3.0");
        userService.setUserCredentials(adminEmail, adminPassword);
    }

    /**
     * Retrieves a user.
     *
     * @param username The user you wish to retrieve.
     * @return A UserEntry object of the retrieved user.
     * @throws AppsForYourDomainException If a Provisioning API specific occurs.
     * @throws ServiceException If a generic GData framework error occurs.
     * @throws IOException If an error occurs communicating with the GData
     * service.
     */
    public UserEntry retrieveUser(String username)
            throws AppsForYourDomainException, ServiceException, IOException {

        LOGGER.log(Level.INFO, "Retrieving user ''{0}'' from Google Apps.", username);

        URL retrieveUrl = new URL(domainUrlBase + "user/" + SERVICE_VERSION + "/" + username);
        return userService.getEntry(retrieveUrl, UserEntry.class);
    }

    /**
     * Updates a user.
     *
     * @param username The user to update.
     * @param userEntry The updated UserEntry for the user.
     * @return A UserEntry object of the newly updated user.
     * @throws AppsForYourDomainException If a Provisioning API specific occurs.
     * @throws ServiceException If a generic GData framework error occurs.
     * @throws IOException If an error occurs communicating with the GData
     * service.
     */
    public UserEntry updateUser(String username, UserEntry userEntry)
            throws AppsForYourDomainException, ServiceException, IOException {

        LOGGER.log(Level.INFO, "Updating user ''{0}'' in Google Apps.", username);

        URL updateUrl = new URL(domainUrlBase + "user/" + SERVICE_VERSION + "/" + username);
        return userService.update(updateUrl, userEntry);
    }

    public boolean setPassword(String username, String passwd) throws AppsForYourDomainException, ServiceException, IOException {
        ShaPasswordEncoder pwe = new ShaPasswordEncoder();
        String passwordHashFunction = pwe.getAlgorithm();
        passwd = pwe.encodePassword(passwd, null);

        UserEntry entry = retrieveUser(username);
        Login login = entry.getLogin();

        login.setPassword(passwd);
        if (passwordHashFunction != null) {
            login.setHashFunctionName(passwordHashFunction);
        }
        login.setChangePasswordAtNextLogin(false);

        @SuppressWarnings("unused")
		UserEntry result = updateUser(username, entry);
        return true;
    }
}
