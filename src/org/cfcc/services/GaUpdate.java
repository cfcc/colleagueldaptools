/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cfcc.services;

import com.google.gdata.data.appsforyourdomain.AppsForYourDomainErrorCode;
import com.google.gdata.data.appsforyourdomain.AppsForYourDomainException;
import com.google.gdata.util.ServiceException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jfriant
 */
public class GaUpdate implements Runnable {

    final private String admin_email;
    final private String admin_password;
    final private String domain;
    final private String username;
    final private String password;
    final private String track_id;
    
    static public int MAX_RETRIES = 3;

    public GaUpdate(String admin_email, String admin_pw, String domain, String username, String user_pw, String track_id) {
        this.admin_email = admin_email;
        this.admin_password = admin_pw;
        this.domain = domain;
        this.username = username;
        this.password = user_pw;
        this.track_id = track_id;
    }

    @Override
	public void run() {
        AppsForYourDomainClient client;
        for (int i=0; i<MAX_RETRIES; i++) {
            try {
                client = new AppsForYourDomainClient(
                        admin_email,
                        admin_password,
                        domain);
                client.setPassword(username, password);
                break;
            } catch (AppsForYourDomainException ex) {
                if (ex.getErrorCode() == AppsForYourDomainErrorCode.ServerBusy) {
                    log(Level.WARNING, "Google Apps server is busy, sleeping...", ex);
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException ex1) {
                        log(Level.SEVERE, "Resuming processing of gdata after interruption", ex1);
                    }
                } else {
                    log(Level.SEVERE, "Error sending password to Google Apps", ex);
                    break;
                }
            } catch (ServiceException ex) {
                log(Level.SEVERE, "Error sending password to Google Apps", ex);
                break;
            } catch (IOException ex) {
                log(Level.SEVERE, "Error sending password to Google Apps", ex);
                break;
            } catch (Exception ex) {
                log(Level.SEVERE, "Error sending password to Google Apps", ex);
                break;
            }
        }
        log(Level.INFO, "Completed update of Google Apps");
    }
    
    private void log(Level level, String msg) {
        String params[] = {track_id, msg};
        Logger.getLogger(GaUpdate.class.getName()).log(level, "(track_id={0}) {1}", params);
    }
    
    private void log(Level level, String msg, Throwable ex) {
        String my_msg = "(track_id=" + track_id + ") " + msg;
        Logger.getLogger(GaUpdate.class.getName()).log(level, my_msg, ex);
    }
}
