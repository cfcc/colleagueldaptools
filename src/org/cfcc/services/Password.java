/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.services;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import org.apache.commons.codec.binary.Base64;
import org.cfcc.SystemUnavailableException;

/**
 * A class to provide SHA password encryption.
 *
 * Downloaded from: <a href="http://www.devarticles.com/c/a/Java/Password-Encryption-Rationale-and-Java-Example/">devarticles.com</a>.
 * <p>
 * From the article:
 * <p>
 * <blockquote>
 * Step 1: The registration servlet will interface with our PasswordService
 * class using this static getInstance() method. Whenever it is invoked, a
 * check will be made to see if an instance of this service class already
 * exists. If so, it will be returned back to the caller (registration
 * servlet). Otherwise, a new instance will be created.<br>
 * <br>
 * Step 2: We are asking Java security API to obtain an instance of a message
 * digest object using the algorithm supplied (in this case, SHA-1 message
 * digest algorithm will be used. Both SHA and SHA-1 refer to the same thing,
 * a revised SHA algorithm). Sun JDK includes JCA (Java Cryptography
 * Architecture) which includes support for SHA algorithm. If your environment
 * does not support SHA, NoSuchAlgorithmException will be thrown.<br><br>
 * <br>
 * Step 3: Feed the data: a) convert the plaintext password (eg, "jsmith")
 * into a byte-representation using UTF-8 encoding format.  b) apply this
 * array to the message digest object created earlier. This array will be used
 * as a source for the message digest object to operate on.<br>
 * <br>
 * Step 4: Do the transformation: generate an array of bytes that represent
 * the digested (encrypted) password value.<br>
 * <br>
 * Step 5: Create a String representation of the byte array representing the
 * digested password value. This is needed to be able to store the password in
 * the database. At this point, the hash value of the plaintext "jsmith" is
 * "5yfRRkrhJDbomacm2lsvEdg4GyY=".<br>
 * <br>
 * Step 6: Return the String representation of the newly generated hash back
 * to our registration servlet so that it can be stored in the database. The
 * user.getPassword() method now returns "5yfRRkrhJDbomacm2lsvEdg4GyY="
 * </blockquote>
 *
 * @author James Shvarts
 */
public final class Password {

    private static Password instance;

    /**
     * Class default constructor, does nothing.
     */
    private Password() {
    }

    /**
     * Returns an encrypted password string as a SSHA hash, prefixing the
     * "{SSHA}" tag for LDAP to recognized it.
     *
     * <p>Salting ideas came from an article at SecurityDocs.com called
     * "Salted hashes demystified - A Primer" by Andres Andreu, 07/06/2005,
     * <a href="http://www.securitydocs.com/library/3439">
     * http://www.securitydocs.com/library/3439</a>
     *
     * <p>Additional information from http://www.acegisecurity.org/acegi-security/xref/org/acegisecurity/providers/ldap/authenticator/LdapShaPasswordEncoder.html
     *
     * @param plaintext the string to be encrypted
     * @return hash the encrypted SHA hash
     * @throws SystemUnavailableException If the SHA message digest is not
     * available in this system.
     */
    public synchronized String encrypt(String plaintext) throws SystemUnavailableException {
        MessageDigest md = null;
        byte[] salt = createRandomSalt();

        try {
            md = MessageDigest.getInstance("SHA"); //step 2
        } catch (NoSuchAlgorithmException e) {
            throw new SystemUnavailableException(e.getMessage());
        }
        try {
            md.update(salt);
            md.update(plaintext.getBytes("UTF-8")); //step 3
        } catch (UnsupportedEncodingException e) {
            throw new SystemUnavailableException(e.getMessage());
        }
        byte[] raw = md.digest(); //step 4
        String hash = "{SSHA}" + (new String(Base64.encodeBase64(concatenate(raw, salt)))); //step 5
        return hash; //step 6
    }

    public synchronized byte[] createRandomSalt() {
        SecureRandom sr = new SecureRandom();
        byte[] salt = new byte[12];
        sr.nextBytes(salt);
        return salt;
    }

    /**
     * Returns the current instance of this class or creates a new one.
     *
     * @return instance an instance of this PasswordService class
     */
    public static synchronized Password getInstance() //step 1
    {
        if (instance == null) {
            return new Password();
        } else {
            return instance;
        }
    }

    /**
     * Combine two byte arrays
     *
     * @param l
     *      first byte array
     * @param r
     *      second byte array
     * @return byte[] combined byte array
     */
    private static byte[] concatenate(byte[] l, byte[] r) {
        byte[] b = new byte[l.length + r.length];
        System.arraycopy(l, 0, b, 0, l.length);
        System.arraycopy(r, 0, b, l.length, r.length);
        return b;
    }

    /**
     * split a byte array in two
     *
     * @param src
     *      byte array to be split
     * @param n
     *      element at which to split the byte array
     * @return byte[][] two byte arrays that have been split
     */
    @SuppressWarnings("unused")
	private static byte[][] split(byte[] src, int n) {
        byte[] l, r;
        if (src == null || src.length <= n) {
            l = src;
            r = new byte[0];
        } else {
            l = new byte[n];
            r = new byte[src.length - n];
            System.arraycopy(src, 0, l, 0, n);
            System.arraycopy(src, n, r, 0, r.length);
        }
        byte[][] lr = {l, r};
        return lr;
    }
}
