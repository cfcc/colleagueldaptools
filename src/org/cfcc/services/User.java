/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.services;

import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import org.cfcc.utils.PwGen;

/**
 * This class shows how to capture attributes in an object. Used to store
 * attributes on the server if the object is not serializeable.
 * <p>
 * Much of this was copied from the examples directory in the JNDI package.
 *
 */
public class User implements DirContext {

    /** the DN of this record */
    String type;
    /** Holds the list of attributes we want to save to this record */
    Attributes myAttrs;

    /**
     * A java object that can be used by the JDI to create a record in the LDAP tree.
     * Customized to create a record from Colleague data
     * @param uid The username (or user ID)
     * @param ou The primary group (Employee, Faculty, Student)
     * @param per_id The Colleague Person ID
     * @throws java.lang.NumberFormatException Thrown if the Person ID is not a number
     */
    public User(String uid, String ou, String per_id) throws NumberFormatException {
        type = uid;

        myAttrs = new BasicAttributes(true);

        Attribute oc = new BasicAttribute("objectclass");
        oc.add("top");
        oc.add("person");
        oc.add("organizationalPerson");
        oc.add("inetOrgPerson");
        oc.add("posixAccount");
        oc.add("shadowAccount");
//       Attribute ouSet = new BasicAttribute("ou");
//       ouSet.add(ou.substring(ou.indexOf("=") + 1));

        @SuppressWarnings("unused")
		String cn = uid;

        myAttrs.put(oc);
        myAttrs.put("uid", uid);
        myAttrs.put("cn", uid);
        myAttrs.put("sn", uid);
        if (!per_id.equals("")) {
            myAttrs.put("employeeNumber", per_id);
        }
        myAttrs.put("userPassword", PwGen.getPassword(8));
        Integer uid_num = new Integer(per_id);
        myAttrs.put("uidNumber", uid_num.toString());
        myAttrs.put("gidNumber", "1000");
        myAttrs.put("homeDirectory", "/home/" + uid);
//        myAttrs.put("shadowFlag", "0");
//        myAttrs.put("shadowExpires")
    }

    /**
     * A function to access a single attribute stored in this object
     * @param name The name of the attribute
     * @return The attribute object list
     */
    @Override
	public Attributes getAttributes(String name) throws NamingException {
        if (!name.equals("")) {
            throw new NameNotFoundException();
        }
        // Return a collection of attributes with only the requested one
        Attributes result = new BasicAttributes();
        result.put(name, myAttrs.get(name));
        return result;
    }

    /**
     * Access only one attribute stored in this object
     * @param name The name of the requested attribute
     * @throws javax.naming.NamingException Thrown if the name does not exist
     * @return The attribute object
     */
    @Override
	public Attributes getAttributes(Name name) throws NamingException {
        return getAttributes(name.toString());
    }

    /**
     * Gets a list of values from the given attribute.
     * @param name The attribute
     * @param ids The list of values to return
     * @throws javax.naming.NamingException Thrown if the name is blank
     * @return 
     */
    @Override
	public Attributes getAttributes(String name, String[] ids)
            throws NamingException {
        if (!name.equals("")) {
            throw new NameNotFoundException();
        }

        Attributes answer = new BasicAttributes(true);
        Attribute target;
        for (int i = 0; i < ids.length; i++) {
            target = myAttrs.get(ids[i]);
            if (target != null) {
                answer.put(target);
            }
        }
        return answer;
    }

    /**
     * Returns the values from a given attribute
     * @param name 
     * @param ids 
     * @throws javax.naming.NamingException 
     * @return 
     */
    @Override
	public Attributes getAttributes(Name name, String[] ids)
            throws NamingException {
        return getAttributes(name.toString(), ids);
    }

    /**
     * 
     * @return 
     */
    @Override
	public String toString() {
        return type;
    }

    // NOT used for this example but need to be defined.
    /**
     * This operation not supported in the class.
     * @param name 
     * @throws javax.naming.NamingException 
     * @return 
     */
    @Override
	public Object lookup(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     * @param name void
     * @throws javax.naming.NamingException 
     * @return void
     */
    @Override
	public Object lookup(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     * @param name void
     * @param obj void
     * @throws javax.naming.NamingException 
     */
    @Override
	public void bind(Name name, Object obj) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     * @param name void
     * @param obj void
     * @throws javax.naming.NamingException 
     */
    @Override
	public void bind(String name, Object obj) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     * @param name void
     * @param obj void
     * @throws javax.naming.NamingException 
     */
    @Override
	public void rebind(Name name, Object obj) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     * @param name void
     * @param obj void
     * @throws javax.naming.NamingException 
     */
    @Override
	public void rebind(String name, Object obj) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public void unbind(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public void unbind(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public void rename(Name oldName, Name newName) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public void rename(String oldName, String newName) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public NamingEnumeration<NameClassPair> list(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public NamingEnumeration<NameClassPair> list(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public NamingEnumeration<Binding> listBindings(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public NamingEnumeration<Binding> listBindings(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public void destroySubcontext(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public void destroySubcontext(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public Context createSubcontext(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public Context createSubcontext(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public Object lookupLink(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public Object lookupLink(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public NameParser getNameParser(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public NameParser getNameParser(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public String composeName(String name, String prefix)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public Name composeName(Name name, Name prefix)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public Object addToEnvironment(String propName, Object propVal)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public Object removeFromEnvironment(String propName)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public Hashtable<?, ?> getEnvironment() throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public void close() throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public void modifyAttributes(Name name, int mod_op, Attributes attrs)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public void modifyAttributes(String name, int mod_op, Attributes attrs)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public void modifyAttributes(Name name, ModificationItem[] mods)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public void modifyAttributes(String name, ModificationItem[] mods)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public void bind(Name name, Object obj, Attributes attrs)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public void bind(String name, Object obj, Attributes attrs)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public void rebind(Name name, Object obj, Attributes attrs)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public void rebind(String name, Object obj, Attributes attrs)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public DirContext createSubcontext(Name name, Attributes attrs)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public DirContext createSubcontext(String name, Attributes attrs)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public DirContext getSchema(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public DirContext getSchema(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public DirContext getSchemaClassDefinition(Name name)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public DirContext getSchemaClassDefinition(String name)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public NamingEnumeration<SearchResult> search(Name name,
            Attributes matchingAttributes,
            String[] attributesToReturn)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public NamingEnumeration<SearchResult> search(String name,
            Attributes matchingAttributes,
            String[] attributesToReturn)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public NamingEnumeration<SearchResult> search(Name name,
            Attributes matchingAttributes)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public NamingEnumeration<SearchResult> search(String name,
            Attributes matchingAttributes)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public NamingEnumeration<SearchResult> search(Name name,
            String filter,
            SearchControls cons)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public NamingEnumeration<SearchResult> search(String name,
            String filter,
            SearchControls cons)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in this class.
     */
    @Override
	public NamingEnumeration<SearchResult> search(Name name,
            String filterExpr,
            Object[] filterArgs,
            SearchControls cons)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public NamingEnumeration<SearchResult> search(String name,
            String filterExpr,
            Object[] filterArgs,
            SearchControls cons)
            throws NamingException {
        throw new OperationNotSupportedException();
    }

    /**
     * This operation not supported in the class.
     */
    @Override
	public String getNameInNamespace() throws NamingException {
        throw new OperationNotSupportedException();
    }
}