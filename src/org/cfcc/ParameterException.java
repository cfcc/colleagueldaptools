/*
 *   Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.cfcc;

/**
 * Custom exception class that is thrown when a command line parameter is wrong
 */

public class ParameterException extends Exception
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -5473959695047082382L;

	/**
     * Class default constructor
     *
     * @param s  the error message
     */
    public ParameterException(String s)
	{
	    super(s);
	}
}
