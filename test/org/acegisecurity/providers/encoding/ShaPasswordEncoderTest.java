/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.acegisecurity.providers.encoding;

import junit.framework.TestCase;

/**
 *
 * @author jfriant
 */
public class ShaPasswordEncoderTest extends TestCase {
    
    public ShaPasswordEncoderTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testShaPaswordEncoder() {
        System.out.println("ShaPasswordEncoder");

        String rawPass = "passmine";
        Object salt = null;
        ShaPasswordEncoder instance = new ShaPasswordEncoder();

        String expResult = "4a1e9137dc8c54538970633e6785f5588c91f476";
        String result = instance.encodePassword(rawPass, salt);
        assertEquals(expResult, result);
        
    }

}
