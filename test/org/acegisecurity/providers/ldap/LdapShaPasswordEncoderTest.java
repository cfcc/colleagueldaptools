/*
 * LdapShaPasswordEncoderTest.java
 * JUnit based test
 *
 * Created on November 26, 2007, 2:49 PM
 * 
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.acegisecurity.providers.ldap;

import junit.framework.*;

/**
 *
 * @author jfriant
 */
public class LdapShaPasswordEncoderTest extends TestCase {

    public LdapShaPasswordEncoderTest(String testName) {
        super(testName);
    }

    @Override
	protected void setUp() throws Exception {
    }

    @Override
	protected void tearDown() throws Exception {
    }

    /**
     * Test of encodePassword method, of class org.acegisecurity.providers.ldap.LdapShaPasswordEncoder.
     */
    public void testEncodePassword() throws Exception {
        System.out.println("encodePassword");

        String rawPass = "password";
        Object salt = null;
        org.acegisecurity.providers.ldap.LdapShaPasswordEncoder instance = new org.acegisecurity.providers.ldap.LdapShaPasswordEncoder();

        String expResult = "{SHA}W6ph5Mm5Pz8GgiULbPgzG37mj9g=";
        String result = instance.encodePassword(rawPass, salt);
        assertEquals(expResult, result);
    }

    /**
     * Test of isPasswordValid method, of class org.acegisecurity.providers.ldap.LdapShaPasswordEncoder.
     */
    public void testIsPasswordValid() throws Exception {
        System.out.println("isPasswordValid");


        String encPass = "{SSHA}KpgP2f4+QZBqWrKCpavRQmuIlKh0b2RYZm4xM3RqWE5rNHBk";
        String rawPass = "password";
        Object salt = null;
        org.acegisecurity.providers.ldap.LdapShaPasswordEncoder instance = new org.acegisecurity.providers.ldap.LdapShaPasswordEncoder();

//        Object salt = new String("todXfn13tjXNk4pd").getBytes();
//        String encPass = instance.encodePassword(rawPass, salt);
//        System.out.println(encPass);

        boolean expResult = true;
        boolean result = instance.isPasswordValid(encPass, rawPass, salt);
        assertEquals(expResult, result);
    }

    /**
     * Test of setForceLowerCasePrefix method, of class org.acegisecurity.providers.ldap.LdapShaPasswordEncoder.
     */
    public void testSetForceLowerCasePrefix() {
        System.out.println("setForceLowerCasePrefix");

        boolean forceLowerCasePrefix = true;
        org.acegisecurity.providers.ldap.LdapShaPasswordEncoder instance = new org.acegisecurity.providers.ldap.LdapShaPasswordEncoder();

        instance.setForceLowerCasePrefix(forceLowerCasePrefix);
    }

    /**
     * Test of createRandomSalt method, of class org.acegisecurity.providers.ldap.LdapShaPasswordEncoder.
     */
    public void testCreateRandomSalt() {
        System.out.println("createRandomSalt");

        org.acegisecurity.providers.ldap.LdapShaPasswordEncoder instance = new org.acegisecurity.providers.ldap.LdapShaPasswordEncoder();

        byte[] result = instance.createRandomSalt();
        assertNotNull(result);
    }

    public void testShaPasswordEncoder() {
        System.out.println("testShaPasswordEncoder");

        String rawPass = "passmine";
        String expResult = "4a1e9137dc8c54538970633e6785f5588c91f476";
        byte[] salt = null;
        org.acegisecurity.providers.encoding.ShaPasswordEncoder instance = new org.acegisecurity.providers.encoding.ShaPasswordEncoder();

        String result = instance.encodePassword(rawPass, salt);
        assertEquals(expResult, result);
    }
}
