/*
 * JUnit based test
 *
 * Created on 16 April 2013
 * 
 * Copyright 2007-2013 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.services;

import java.io.IOException;
import java.util.Properties;

import org.cfcc.CryptoException;

import com.google.gdata.data.appsforyourdomain.AppsForYourDomainException;
import com.google.gdata.util.ServiceException;
import com.vladium.utils.PropertyLoader;

import junit.framework.TestCase;

public class GdataTest extends TestCase {

	private Properties test_prop;

	/** Class constructor */
	public GdataTest(String testName) {
		super(testName);
	}

	@Override
	protected void setUp() throws Exception {
		test_prop = PropertyLoader.loadProperties("org.cfcc.services.GdataTest");
	}

	public void testSetPassword() throws CryptoException,
			AppsForYourDomainException, ServiceException, IOException {
		AppsForYourDomainClient client = new AppsForYourDomainClient(
				test_prop.getProperty("google.admin_email"),
				test_prop.getProperty("google.admin_password"),
				test_prop.getProperty("google.domain"));
		client.setPassword(test_prop.getProperty("test.username"),
				test_prop.getProperty("test.password"));

	}
}
