/*
 * EnvironmentTest.java
 * JUnit based test
 *
 * Created on November 28, 2007, 9:13 AM
 * 
 * Copyright 2007-2011 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.colleague;

import asjava.uniobjects.UniFile;
import asjava.uniobjects.UniFileException;
import junit.framework.*;
import com.vladium.utils.PropertyLoader;
import java.util.ArrayList;
import java.util.Properties;
import org.cfcc.LockedAccountException;
import org.cfcc.colfile.Column;

/**
 * TODO: Create some read-only tests.
 * @author jfriant
 */
public class EnvironmentTest extends TestCase {

    private static final boolean READ_ONLY = false;
    private Environment db;
    private Properties test_prop;

    public EnvironmentTest(String testName) {
        super(testName);
        test_prop = PropertyLoader.loadProperties("org.cfcc.colleague.EnvironmentTest");
    }

    @Override
    protected void setUp() throws Exception {
        if (db == null) {
            db = new Environment(test_prop.getProperty("DB_HOST"), test_prop.getProperty("DB_USER"), test_prop.getProperty("DB_PASSWORD"), test_prop.getProperty("LOCAL_PARAMS"));
            db.connect(test_prop.getProperty("DB_ACCOUNT"));
            db.openAll();
        }
    }

    /**
     * Test of getOEE method, of class org.cfcc.colleague.Environment.
     */
    public void testGetOEE() throws Exception {
        System.out.println("getOEE");

        String per_id = test_prop.getProperty("PERSON_ID");

        String expResult = test_prop.getProperty("UID");
        OrgEntity oe = new OrgEntity(db.uSession);
        oe.setId(per_id);
        OrgEntityEnv oee = new OrgEntityEnv(db.uSession);
        oee.setId(oe.org_entity_env.get());
        String result = oee.username.get();
        assertEquals(expResult, result);
    }

    /**
     * Test of loadData method, of class org.cfcc.colleague.Environment.
     */
    public void testLoadData() throws LockedAccountException {
        System.out.println("loadData");

        String per_id = test_prop.getProperty("PERSON_ID");

        assertNotNull(db.uSession);
        boolean expResult = true;
        boolean result = db.loadData(per_id);
        assertEquals(expResult, result);
    }

    /**
     * Test of values in the loadData method, of class org.cfcc.colleague.Environment.
     */
    public void testLoadDataValues() throws LockedAccountException {
        System.out.println("loadData values");

        String per_id = test_prop.getProperty("PERSON_ID");

        String expValue = test_prop.getProperty("EXP_ZIP");
        if (db.loadData(per_id)) {
            assertEquals(expValue, db.zip);
        } else {
            fail("Error loading data");
        }
    }

    public void testGetSsn() throws LockedAccountException {
        System.out.println("getSsn");

        String person_id = test_prop.getProperty("PERSON_ID");
        String expResult = test_prop.getProperty("EXP_SSN");
        db.loadData(person_id);
        assertEquals(expResult, db.ssn_four);
    }

    /**
     * Test of getContext method, of class org.cfcc.colleague.Environment.
     */
    public void testGetContext() {
        System.out.println("getContext");

        String person_id = test_prop.getProperty("PERSON_ID");

        String expResult = test_prop.getProperty("EXP_CONTEXT");
        String result = db.getContext(person_id);
        assertEquals(expResult, result);
    }

    /**
     * Test of setSecret method, of class org.cfcc.colleague.Environment.
     */
    public void testSetSecretParams() throws Exception {
        System.out.println("setSecretParams");

        String person_id = test_prop.getProperty("PERSON_ID");
        String secret_in = test_prop.getProperty("SECRET");

        assertFalse(READ_ONLY);
        db.setSecret(person_id, Environment.security_questions[6], secret_in);
    }

    /**
     * Test setting a secret when no record exists
     */
    public void testSetNewSecretParams() throws Exception {
        System.out.println("setNewSecretParams");

        String params_fn = test_prop.getProperty("LOCAL_PARAMS");
        String person_id = test_prop.getProperty("PERSON_ID");
        String secret_in = test_prop.getProperty("SECRET");

        assertFalse(READ_ONLY);
        try {
            LocalParams local_params = new LocalParams(db.uSession, params_fn);
            UniFile fd = local_params.open();
            fd.deleteRecord(person_id);
            fd.close();
        } catch (UniFileException ex) {
            System.err.println("setNewSecretParams - " + ex);
            System.err.println("setNewSecretParams - No record to delete");
        }

        db.setSecret(person_id, Environment.security_questions[6], secret_in);
    }

    /**
     * Test of checkSecret method, of class org.cfcc.colleague.Environment.
     */
    public void testCheckSecretParams() throws Exception {
        System.out.println("checkSecretParams");

        db.loadSecurityQuestion(test_prop.getProperty("PERSON_ID"));
        String secret_in = test_prop.getProperty("SECRET");

        boolean expResult = true;
        boolean result = db.checkSecret(secret_in);
        assertEquals(expResult, result);
    }

    public void testSetSecretPerson() throws Exception {
        System.out.println("setSecretPerson");

        // Reopen the database without specifying the local params file to use
        // the PERSON file instead.
        db.close();
        db = new Environment(test_prop.getProperty("DB_HOST"), test_prop.getProperty("DB_USER"), test_prop.getProperty("DB_PASSWORD"));
        db.connect(test_prop.getProperty("DB_ACCOUNT"));
        db.openAll();

        String person_id = test_prop.getProperty("PERSON_ID");
        String secret_in = test_prop.getProperty("SECRET");

        assertFalse(READ_ONLY);
        db.setSecret(person_id, Environment.security_questions[6], secret_in);
    }

    public void testCheckSecretPerson() throws Exception {
        System.out.println("checkSecretPerson");

        db.close();
        db = new Environment(test_prop.getProperty("DB_HOST"), test_prop.getProperty("DB_USER"), test_prop.getProperty("DB_PASSWORD"));
        db.connect(test_prop.getProperty("DB_ACCOUNT"));
        db.openAll();

        db.loadSecurityQuestion(test_prop.getProperty("PERSON_ID"));
        String secret_in = test_prop.getProperty("SECRET");

        boolean expResult = true;
        boolean result = db.checkSecret(secret_in);
        assertEquals(expResult, result);
    }
    
    /**
     * Test the error handling when the security questions are missing.
     * The checkSecret function should still return true when the record is not 
     * found in X810.PARAMS.
     */
    public void testMissingSecurityQuestion() throws Exception {
        System.out.println("testMissingSecurityQuestion");
        String bad_id = "00BAD00";
        String secret_in = test_prop.getProperty("SECRET");

        db.loadSecurityQuestion(bad_id);

        boolean expResult = true;
        boolean result = db.checkSecret(secret_in);
        assertEquals(expResult, result);
}
    /**
     * Test of getOfficeCodes method, of class org.cfcc.colleague.Environment.
     */
    public void testGetOfficeCodes() throws Exception {
        System.out.println("getOfficeCodes");

        String per_id = test_prop.getProperty("PERSON_ID");

        ArrayList<String> result = db.getOfficeCodes(per_id);
        assertFalse(result.isEmpty());
    }

    /**
     * Test of setPasswordDate method, of class org.cfcc.colleague.Environment.
     */
    public void testSetPasswordDate() {
        System.out.println("setPasswordDate");

        assertFalse(READ_ONLY);
        db.setPasswordDate(test_prop.getProperty("PERSON_ID"));
    }

    public void testSetPasswordHistory() throws Exception {
        System.out.println("setPasswordHistory");
        assertFalse(READ_ONLY);
        String testPasswd = "{SSHA}KpgP2f4+QZBqWrKCpavRQmuIlKh0b2RYZm4xM3RqWE5rNHBk"; // string is "password"
        db.setPasswordHistory(test_prop.getProperty("PERSON_ID"), testPasswd);
    }

    public void testCheckPasswordHistory() {
        System.out.println("checkPasswordHistory");
        assertFalse(READ_ONLY);

        String per_id = test_prop.getProperty("PERSON_ID");
        String testValue = "password";
        boolean result = db.checkPasswordHistory(per_id, testValue);
        assertTrue(result);

        testValue = "wrong password";
        result = db.checkPasswordHistory(per_id, testValue);
        assertFalse(result);
    }

    public void testLockedRecord() throws Exception {
        System.out.println("lockedRecord");

        String params_fn = test_prop.getProperty("LOCAL_PARAMS");
        String person_id = test_prop.getProperty("PERSON_ID");
        String secret_in = test_prop.getProperty("SECRET");
        LocalParams local_params = new LocalParams(db.uSession, params_fn);
        UniFile fd = local_params.open();

        assertFalse(READ_ONLY);
        try {
            fd.lockRecord(person_id, Column.EXCLUSIVE_UPDATE);
            db.setSecret(person_id, Environment.security_questions[6], secret_in);
        } catch (UniFileException ex) {
            System.err.println("setNewSecretParams - " + ex);
            System.err.println("setNewSecretParams - No record to delete");
        } finally {
            fd.unlockRecord();
            fd.close();
        }
    }

    public void testReconnect() throws Exception {
        System.out.println("reconnect");

        String per_id = test_prop.getProperty("PERSON_ID");

        OrgEntity oe = new OrgEntity(db.uSession);
        oe.setId(per_id);

        db.close();
        db.disconnect();

        String result = oe.org_entity_env.get();

        assertNotNull(result);
    }
    
    public void testPersonBySsn() throws Exception {
    	System.out.println("personBySsn");
        String per_ssn = test_prop.getProperty("PERSON_SSN");
    	String expected_id = test_prop.getProperty("PERSON_ID");
    	String person_id = db.getPersonBySsn(per_ssn, false);
    	assertEquals(expected_id, person_id);

    	String last_name = test_prop.getProperty("PERSON_LAST");
    	String expected_username =  test_prop.getProperty("UID");
    	String username = db.getUsername(person_id, last_name);
    	assertEquals(expected_username, username);
    }
}
