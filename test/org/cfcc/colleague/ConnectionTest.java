/*
 * ConnectionTest.java
 * JUnit based test
 *
 * Created on February 25, 2008, 11:27 AM
 *
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.colleague;

import asjava.uniobjects.UniSession;
import com.vladium.utils.PropertyLoader;
import java.io.FileInputStream;
import java.net.Socket;
import java.util.Properties;
import junit.framework.*;

/**
 *
 * @author jfriant
 */
public class ConnectionTest extends TestCase {

    private static final String SERVER_ADDRESS = "shamash";
    private Environment db;
    private Properties passwd_prop;
    private Properties test_prop;

    public ConnectionTest(String testName) {
        super(testName);
    }

    @Override
	protected void setUp() throws Exception {
        test_prop = PropertyLoader.loadProperties("org.cfcc.colleague.EnvironmentTest");
        passwd_prop = new Properties();
        passwd_prop.load(new FileInputStream(test_prop.getProperty("PROPERTIES_PATH")));
        db = new Environment(test_prop.getProperty("DB_HOST"), test_prop.getProperty("DB_USER"), passwd_prop.getProperty("db.password"));
    }

    public void testSocket() throws Exception {
        System.out.println("Connecting to unidata...");

        System.out.println("testSocket");
        UniSession uSession = new UniSession();
        int port = uSession.connection.getPort();
        System.out.println("Testing connection to " + SERVER_ADDRESS + " on port " + port + "...");
        Socket test_socket = new Socket(SERVER_ADDRESS, port);
        if (test_socket.isConnected()) {
            System.out.println("Connection successful");
            test_socket.close();
        } else {
            throw new Exception("Host connection closed prematurely");
        }
    }

    public void testConnect() throws Exception {
        System.out.println("testConnect");
        db.connect(test_prop.getProperty("DB_ACCOUNT"));
    }
}
