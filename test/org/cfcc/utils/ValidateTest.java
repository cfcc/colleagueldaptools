/*
 * ValidateTest.java
 * JUnit based test
 *
 * Created on November 28, 2007, 9:51 AM
 * 
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.utils;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Properties;
import junit.framework.*;

/**
 *
 * @author jfriant
 */
public class ValidateTest extends TestCase {

    public ValidateTest(String testName) {
        super(testName);
    }

    /**
     * Test of sanitize method, of class org.cfcc.utils.Validate.
     */
    public void testSanitize() {
        System.out.println("sanitize");

        String str_in = "user@example.com";

        String expResult = "userexamplecom";
        String result = Validate.sanitize(str_in);
        assertEquals(expResult, result);
    }

    /**
     * Test of numbersOnly method, of class org.cfcc.utils.Validate.
     */
    public void testNumbersOnlyGood() {
        System.out.println("numbersOnly good");

        String in = "1234";

        String expResult = "1234";
        String result = Validate.numbersOnly(in);
        assertEquals(expResult, result);
    }

    /**
     * Test of numbersOnly method, of class org.cfcc.utils.Validate.
     */
    public void testNumbersOnlyBad() {
        System.out.println("numbersOnly bad");

        String in = "ab12";

        String expResult = "12";
        String result = Validate.numbersOnly(in);
        assertEquals(expResult, result);
    }

    /**
     * Test of numbersOnly method, of class org.cfcc.utils.Validate.
     */
    public void testNumbersOnlyLong() {
        System.out.println("numbersOnly long");

        String in = "1234567";

        String expResult = "1234";
        String result = Validate.numbersOnly(in, 4);
        assertEquals(expResult, result);
    }

    /**
     * Test of numbersOnly method, of class org.cfcc.utils.Validate.
     */
    public void testNumbersOnlyShort() {
        System.out.println("numbersOnly short");

        String in = "123";

        String expResult = "123";
        String result = Validate.numbersOnly(in, 4);
        assertEquals(expResult, result);
    }

    /**
     * Test a checkPassword that should validate.
     */
    public void testPasswordGood() {
        System.out.println("validate good password");

        Properties test_props = new Properties();
        test_props.setProperty("minStrength", "3");
        test_props.setProperty("minLength", "8");
        String in = "test123Four";
        boolean expResult = true;

        Validate instance = new Validate(test_props);
        ArrayList<?> result = instance.checkPassword(in, in);
        if (!result.isEmpty()) {
            ListIterator<?> errors = result.listIterator();
            System.out.println("     Password has errors:");
            while (errors.hasNext()) {
                System.out.println("     * " + errors.next().toString());
            }
            System.out.println("");
        }
        assertEquals(expResult, result.isEmpty());
    }

    public void testPasswordBad() {
        System.out.println("validate bad password");

        Properties test_props = new Properties();
        test_props.setProperty("servlet.password.min_strength", "3");
        test_props.setProperty("servlet.password.min_length", "8");
        String in = "testabcd";
        String in_repeat = "testabcd";
        boolean expResult = false;

        Validate instance = new Validate(test_props);
        ArrayList<?> result = instance.checkPassword(in, in_repeat);
        if (!result.isEmpty()) {
            ListIterator<?> errors = result.listIterator();
            System.out.println("     Password does not meet requirements:");
            while (errors.hasNext()) {
                System.out.println("     * " + errors.next().toString());
            }
            System.out.println("");
        }
        assertEquals(expResult, result.isEmpty());
    }
}
