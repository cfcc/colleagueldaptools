/*
 * CryptoTest.java
 * JUnit based test
 *
 * Created on December 27, 2007, 9:05 AM
 *
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.utils;

import junit.framework.*;
import org.cfcc.CryptoException;

/**
 *
 * @author jfriant
 */
public class CryptoTest extends TestCase {

    private static final String PLAIN_RESULT = "testpwd1";
    private static final String CIPHER_RESULT = "{AES}U2FsdGVkX19uRvCdT8Bs7Zf93FAqI7yC28ceC9N4RNA=";
    private static final String CIPHER_WRONG = "{AES}U2FsdGVkX19ogA85mMry8r0Djxa7toxpVe6BNqYKv/M=";

    public CryptoTest(String testName) {
        super(testName);
    }

    public void testBoth() {
        System.out.println("both encrypt and decrypt");

        Crypto instance = new Crypto();
        String enResult = null;
        String deResult = null;

        try {
            enResult = instance.encrypt(PLAIN_RESULT);
            System.out.println("encrypt result=[" + enResult + "]");

            instance = new Crypto();
            deResult = instance.decrypt(enResult);
            String result = new String(deResult);
            System.out.println("decrypt result=[" + result + "]");
            assertEquals(PLAIN_RESULT, result);
        } catch (CryptoException ex) {
            fail(ex.toString());
        }
    }

    /**
     * Test of encrypt method, of class org.cfcc.utils.Crypto.
     */
    public void testEncrypt() {
        System.out.println("encrypt");

        String result = null;
        String ldap_password_in = PLAIN_RESULT;
        Crypto instance = new Crypto();

        try {
            result = instance.encrypt(ldap_password_in);
        } catch (CryptoException ex) {
            fail(ex.toString());
        }
        System.out.println("testEncrypt() result=[" + result + "]");
        assertNotNull(result);
    }

    /**
     * Test of decrypt method, of class org.cfcc.utils.Crypto.
     */
    public void testDecrypt() {
        System.out.println("decrypt");

        String result = null;
        String ciphertext = CIPHER_RESULT;
        Crypto instance = new Crypto();

        String expResult = PLAIN_RESULT;
        try {
            result = instance.decrypt(ciphertext);
        } catch (CryptoException ex) {
            fail(ex.toString());
        }
        assertEquals(expResult, result);

        ciphertext = CIPHER_WRONG;
        try {
            result = instance.decrypt(ciphertext);
        } catch (CryptoException ex) {
            fail(ex.toString());
        }
        System.out.println("testDecrypt(): decrypt=[" + result + "]");
        assertNotSame(expResult, result);
    }
}
